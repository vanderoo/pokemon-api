'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('pokemons', [
      {nama: 'bulbasaur', createdAt: new Date(), updatedAt: new Date() },
      {nama: 'ivysaur', createdAt: new Date(), updatedAt: new Date() },
      {nama: 'venusaur', createdAt: new Date(), updatedAt: new Date() },
      {nama: 'charmander', createdAt: new Date(), updatedAt: new Date() },
      {nama: 'charmelon', createdAt: new Date(), updatedAt: new Date() },
      {nama: 'charizard', createdAt: new Date(), updatedAt: new Date() },
      {nama: 'squirtle', createdAt: new Date(), updatedAt: new Date() },
      {nama: 'wartortle', createdAt: new Date(), updatedAt: new Date() },
      {nama: 'blastoise', createdAt: new Date(), updatedAt: new Date() },
      {nama: 'caterpie', createdAt: new Date(), updatedAt: new Date() },
    ]);
    await queryInterface.bulkInsert('pokemon_skills', [
      {nama: 'overgrow', pokemonId: 1, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'chlorophyll', pokemonId: 1, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'overgrow', pokemonId: 2, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'chlorophyll', pokemonId: 2, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'overgrow', pokemonId: 3, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'chlorophyll', pokemonId: 3, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'blaze', pokemonId: 4, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'solar-power', pokemonId: 4, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'blaze', pokemonId: 5, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'solar-power', pokemonId: 5, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'blaze', pokemonId: 6, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'solar-power', pokemonId: 6, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'torrent', pokemonId: 7, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'rain-dish', pokemonId: 7, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'torrent', pokemonId: 8, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'rain-dish', pokemonId: 8, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'torrent', pokemonId: 9, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'rain-dish', pokemonId: 9, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'shield-dust', pokemonId: 10, createdAt: new Date(), updatedAt: new Date() },
      {nama: 'run-away', pokemonId: 10, createdAt: new Date(), updatedAt: new Date() },
    ]);
    await queryInterface.bulkInsert('pokemon_details', [
      { pokemonId:1, height: 0.7, weight: 6.9, type: "Grass", hp: 45, attack: 49, defense: 49,
        special_attack: 65, special_defense: 65, speed: 45, createdAt: new Date(), updatedAt: new Date() },
      { pokemonId:2, height: 1.0, weight: 13.0, type: "Grass", hp: 60, attack: 62, defense: 63,
        special_attack: 80, special_defense: 80, speed: 60, createdAt: new Date(), updatedAt: new Date() },
      { pokemonId:3, height: 2.0, weight: 100.0, type: "Grass", hp: 80, attack: 82, defense: 83,
        special_attack: 100, special_defense: 100, speed: 80, createdAt: new Date(), updatedAt: new Date() },
      { pokemonId:4, height: 0.6, weight: 8.5, type: "Fire", hp: 39, attack: 52, defense: 43,
        special_attack: 60, special_defense: 50, speed: 65, createdAt: new Date(), updatedAt: new Date() },
      { pokemonId:5, height: 1.1, weight: 19.0, type: "Fire", hp: 58, attack: 64, defense: 58,
        special_attack: 80, special_defense: 65, speed: 80, createdAt: new Date(), updatedAt: new Date() },
      { pokemonId:6, height: 1.7, weight: 90.5, type: "Fire", hp: 78, attack: 84, defense: 78,
        special_attack: 109, special_defense: 85, speed: 100, createdAt: new Date(), updatedAt: new Date() },
      { pokemonId:7, height: 0.5, weight: 9.0, type: "Water", hp: 44, attack: 48, defense: 65,
        special_attack: 50, special_defense: 64, speed: 43, createdAt: new Date(), updatedAt: new Date() },
      { pokemonId:8, height: 1.0, weight: 22.5, type: "Water", hp: 59, attack: 63, defense: 80,
        special_attack: 65, special_defense: 80, speed: 50, createdAt: new Date(), updatedAt: new Date() },
      { pokemonId:9, height: 1.6, weight: 85.5, type: "Water", hp: 79, attack: 83, defense: 100,
        special_attack: 85, special_defense: 105, speed: 70, createdAt: new Date(), updatedAt: new Date() },
      { pokemonId:10, height: 0.3, weight: 2.9, type: "Bug", hp: 45, attack: 30, defense: 35,
        special_attack: 20, special_defense: 20, speed: 45, createdAt: new Date(), updatedAt: new Date() },
    ]);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('pokemons', null, {});
    await queryInterface.bulkDelete('pokemon_skills', null, {});
    await queryInterface.bulkDelete('pokemon_details', null, {});
  }
};
