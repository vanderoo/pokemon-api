import PokemonService from "../services/PokemonService.js";

class PokemonController {
    constructor(server) {
        this.server = server;
        this.PokemonService = new PokemonService(this.server);
    }

    async getAllpokemon (req, res) {

        try {
            const pokemons = await this.PokemonService.getAllPokemon()

            return res.status(200).json({data: pokemons})

        } catch (error) {
            return res.status(500).json({message: error.message})
        }
    }

    async getMyPokemon (req, res) {
        try {
            const myPokemons = await this.PokemonService.getMyPokemon();
            if(myPokemons === -1){
                return  res.status(404).json({message: "Data not found"})
            }
            return res.status(200).json({data: myPokemons})
        } catch (error) {
            return res.status(500).json({message: error.message})
        }
    }

    async getPokemonDetail (req, res) {
        try {
            const {id} = req.params;
            const pokemonData = await this.PokemonService.getPokemonDetail(id)
            const image = await this.PokemonService.getImageURL()
            const result = {
                id: pokemonData.id,
                nama: pokemonData.nama,
                type: pokemonData.pokemon_detail.type,
                height: pokemonData.pokemon_detail.height, 
                weight: pokemonData.pokemon_detail.weight,
                imageURL: image, 
                skills: pokemonData.pokemon_skills.map(skill => ({ 
                    id: skill.id,
                    nama: skill.nama
                })),
                statistics: {
                    hp: pokemonData.pokemon_detail.hp, 
                    attack: pokemonData.pokemon_detail.attack, 
                    defense: pokemonData.pokemon_detail.defense, 
                    special_attack: pokemonData.pokemon_detail.special_attack, 
                    special_defense: pokemonData.pokemon_detail.special_defense,
                    speed: pokemonData.pokemon_detail.speed 
                }
            };

            return res.status(200).json(result)

        } catch (error) {
            return res.status(500).json({message: error.message})
        }
    }

    async catchPokemon (req, res) {
        try {
            const{id} = req.params
            const capturedPokemon = await this.PokemonService.catchPokemon(id);

            if(capturedPokemon === 1){
                return res.status(400).json({message: "Pokemon has been Captured"})
            }
            if(capturedPokemon === 2){
                return res.status(400).json({message: "The Pokemon doesn't exist"})
            }

            return res.status(200).json({message: "Pokemon Captured"})
        } catch (error) {
            return res.status(500).json({message: error.message})
        }
    }

    async releasePokemon (req, res) {
        try {
            const{id} = req.params
            const data = await this.PokemonService.releasePokemon(id);
            if(data === 1){
                return res.status(400).json({message: "The Pokemon hasn't been caught yet"})
            }
            if(data === 2){
                return res.status(400).json({message: "The Pokemon doesn't exist"})
            }
            return res.status(200).json({message: "Pokemon Released"})
        } catch (error) {
            return res.status(500).json({message: error.message})
        }
    }

} 

export default PokemonController