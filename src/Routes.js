import PokemonController from "./controllers/PokemonController.js";

class Routes {
    constructor(server){
        this.server = server
        this.router = this.server.router
        this.PokemonController = new PokemonController(this.server)

        this.setupRoutes()
    }

    setupRoutes(){
        this.router.get('/', (req,res)=>{
            res.status(200).json({
                status: "OK"
            })
        });

        this.router.get('/pokemons', this.PokemonController.getAllpokemon.bind(this.PokemonController))
        this.router.get('/pokemons/:id', this.PokemonController.getPokemonDetail.bind(this.PokemonController))
        this.router.post('/pokemons/:id', this.PokemonController.catchPokemon.bind(this.PokemonController))
        this.router.delete('/pokemons/:id', this.PokemonController.releasePokemon.bind(this.PokemonController))
        this.router.get('/my-pokemon', this.PokemonController.getMyPokemon.bind(this.PokemonController))
    }
}

export default Routes