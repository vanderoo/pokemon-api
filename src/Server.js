import express from "express";
import dotenv from "dotenv";
import cors from 'cors'

import Database from "./config/Database.js";
import Routes from "./Routes.js";

class Server {
  constructor(){
    dotenv.config();
    this.env = process.env;
    this.router = express.Router();
    this.model = new Database(this);
  }

  async connectDatabase(){
    const isModelConnected = this.model.connect();
    if(isModelConnected === -1) return;
    this.run();
  }

  run(){
    new Routes(this)

    this.APP = express()

    this.APP.use(express.json())
    this.APP.use(cors())

    this.APP.use(this.router)

    this.APP.listen(this.env.PORT, ()=>{
      console.log(`Server running on port ${this.env.PORT}`)
    })

  }
}

const serverInstance = new Server()

serverInstance.connectDatabase()
