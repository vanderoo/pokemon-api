import Pokemon from "../models/Pokemon.js";
import PokemonDetail from "../models/PokemonDetail.js";
import PokemonSKill from "../models/PokemonSkill.js";
import CapturedPokemon from "../models/CapturedPokemon.js";

import fs from 'fs';
import path from 'path'
import { fileURLToPath } from "url";

const __dirname = path.dirname(fileURLToPath(import.meta.url));

class PokemonService {
    constructor(server) {
        this.server = server;
        this.PokemonModel = new Pokemon(this.server).table;
        this.PokemonDetailModel = new PokemonDetail(this.server).table;
        this.PokemonSkillModel = new PokemonSKill(this.server).table;
        this.CapturedPokemonModel = new CapturedPokemon(this.server).table;

        this.PokemonModel.hasOne(this.PokemonDetailModel);
        this.PokemonModel.hasOne(this.CapturedPokemonModel);
        this.PokemonModel.hasMany(this.PokemonSkillModel);
        this.PokemonDetailModel.belongsTo(this.PokemonModel);
        this.PokemonSkillModel.belongsTo(this.PokemonModel);
        this.CapturedPokemonModel.belongsTo(this.PokemonModel);
    }

    async getAllPokemon(){
        try {
            const pokemons = await this.PokemonModel.findAll();
            const image = await this.getImageURL()
            const data = pokemons.map(pokemon => ({
                id: pokemon.id,
                nama: pokemon.nama,
                imageURL: image
            }));
        
            return data;
        } catch (error) {
            throw error;
        }
    }

    async getMyPokemon(){
        try {
            const myPokemons = await this.CapturedPokemonModel.findAll({
                include: [{
                    model: this.PokemonModel,
                    attributes: ['nama']
                }]
            });
            if(!myPokemons || myPokemons.length === 0){
                return -1;
            }
            const image = await this.getImageURL();
            const data = myPokemons.map(pokemon =>({
                id: pokemon.pokemonId,
                nama: pokemon.pokemon.nama,
                imageURL: image
            }));

            return data;
        } catch (error) {
            throw error
        }
    }

    async getPokemonDetail(id){
        try {
            const pokemonData = await this.PokemonModel.findByPk(id,{
                include: [
                    {
                        model: this.PokemonDetailModel,
                        attributes: ['height', 'weight', 'type', 'hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed'],
                    },
                    {
                        model: this.PokemonSkillModel,
                        attributes: ['id', 'nama'],
                    },
                ],
            });

            return pokemonData

        } catch (error) {
            throw error
        }
    }

    async catchPokemon(id){
        try {
            const pokemon = await this.CapturedPokemonModel.findOne({
                where: {pokemonId: id}
            });

            if(pokemon){
                return 1;
            }

            const isPokemonExist = await this.PokemonModel.findByPk(id);

            if(!isPokemonExist){
                return 2;
            }

            const capturedPokemon = await this.CapturedPokemonModel.create({
                pokemonId: id
            });
            return capturedPokemon;
        } catch (error) {
            throw error
        }
    }

    async releasePokemon(id){
        try {
            const isPokemonExist = await this.PokemonModel.findByPk(id);

            if(!isPokemonExist){
                return 2;
            }
            
            const pokemon = await this.CapturedPokemonModel.findOne({
                where: {pokemonId: id}
            });

            if(!pokemon){
                return 1;
            }

            await this.CapturedPokemonModel.destroy({
                where: {
                    pokemonId: id
                }
            });

            return 3;

        } catch (error) {
           throw error 
        }
    }

    async getImageURL(){
        const imagePath = path.join(__dirname, '..', 'public', 'images', 'bulbasaur.png');
        try {
            const data = await fs.promises.readFile(imagePath);
            const base64Image = Buffer.from(data).toString('base64');
            const base64ImageUrl = `data:image/png;base64,${base64Image}`;
            return base64ImageUrl;
        } catch (error) {
            throw error;
        }
    }

}

export default PokemonService