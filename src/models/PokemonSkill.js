import {DataTypes} from 'sequelize'
import Pokemon from './Pokemon.js'

class PokemonSKill {
    constructor(server) {
        const PokemonModel = new Pokemon(server).table;
        this.table = server.model.db.define('pokemon_skills', {
            nama: { 
                type: DataTypes.STRING,
                allowNull: false,
            },
            pokemonId: { 
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: PokemonModel,
                    key: 'id',
                }
            }
        });
    }
}

export default PokemonSKill