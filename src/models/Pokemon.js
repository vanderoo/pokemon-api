import {DataTypes} from 'sequelize'

class Pokemon {
    constructor(server) {
        this.table = server.model.db.define('pokemons', {
            nama: { 
                type: DataTypes.STRING,
                allowNull: false,
            },
        });
    }
}

export default Pokemon