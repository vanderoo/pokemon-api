import {DataTypes} from 'sequelize'
import Pokemon from './Pokemon.js'

class CapturedPokemon {
    constructor(server) {
        const PokemonModel = new Pokemon(server).table;
        this.table = server.model.db.define('captured_pokemons', {
            pokemonId: { 
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: PokemonModel,
                    key: 'id',
                }
            }
        });
    }
}

export default CapturedPokemon