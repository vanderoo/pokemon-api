import {DataTypes} from 'sequelize'
import Pokemon from './Pokemon.js'

class PokemonDetail {
    constructor(server) {
        const PokemonModel = new Pokemon(server).table;
        this.table = server.model.db.define('pokemon_details', {
            pokemonId: { 
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: PokemonModel,
                    key: 'id',
                }
            },
            height: { 
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            weight: { 
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            type: { 
                type: DataTypes.STRING,
                allowNull: false,
            },
            hp: { 
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            attack: { 
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            defense: { 
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            special_attack: { 
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            special_defense: { 
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            speed: { 
                type: DataTypes.INTEGER,
                allowNull: false,
            },
        });
    }
}

export default PokemonDetail